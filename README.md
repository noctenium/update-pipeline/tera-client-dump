# Tera File Dump

> Repository containing the most recent tera client binary files used for triggering the RE pipeline

## Contents

| File | Description |
| --- | --- |
| TERA_dump.exe | the dumped client binary |
| ReleaseRevison.txt | used for version tracking|

## Disclaimer

As this repository is directly releated to reverse engineering and it contains the actual game binaries it should never be public!
